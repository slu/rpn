#!/usr/bin/env perl

#
# A very simple RPN calculator for the terminal.
#
# It supports four operators: addition (+), subtraction (-),
# multiplication (*), and division (/).
#
# Example:
#
#     $ ./rpn.pl
#     2 5 8 +
#     [2]
#     [2,5]
#     [2,5,8]
#     [2,13]
#     * 6 - 4 /
#     [26]
#     [26,6]
#     [20]
#     [20,4]
#     [5]
#     5
#
# See https://en.wikipedia.org/wiki/Reverse_Polish_notation
#

use strict;
use warnings;

my @stack;
my %vars;


sub resolve {
    $_[0] =~ /[a-zA-Z]/ ? $vars{$_[0]} : $_[0];
}

my %op = (
    'binary' => {
        '=' => sub { $vars{$_[0]} = resolve($_[1]) },

        '+' => sub { resolve($_[1]) + resolve($_[0]) },
        '-' => sub { resolve($_[1]) - resolve($_[0]) },
        '*' => sub { resolve($_[1]) * resolve($_[0]) },
        '/' => sub { resolve($_[1]) / resolve($_[0]) },

        'x' => sub { push @stack, $_[0]; $_[1] }

    },
    'unary' => {
        'a' => sub { abs resolve($_[0]) },
        'i' => sub { int resolve($_[0]) },
        'n' => sub {   - resolve($_[0]) }
    }
   );

sub dump_state {
    printf STDERR "[%s]\n", join(',', @stack);
    while (my ($var, $val) = each %vars) {
        printf STDERR "%s = %s\n", $var, $val;
    }
}

my $binary_ops = join "", sort keys %{$op{binary}};
my $unary_ops  = join "", sort keys %{$op{unary}};

while (my $line = <>) {
    for my $op (split(/\s+/, $line)) {
        if ($op =~ /^[$binary_ops]$/) {
            push @stack, $op{binary}->{$op}->(pop @stack, pop @stack);
        } elsif ($op =~ /^[$unary_ops]$/) {
            push @stack, $op{unary}->{$op}->(pop @stack);
        } else {
            push @stack, $op;
        }
        dump_state();
    }
}

printf "%s\n", pop @stack;
