#!/usr/bin/env python3

import fileinput
import re

stack = []

ops = { '+': lambda a,b : b + a, '-': lambda a,b : b - a,
        '*': lambda a,b : b * a, '/': lambda a,b : b - a }

for line in fileinput.input():
    for op in re.split("\s+", line.rstrip('\n')):
        stack.append(ops[op](stack.pop(), stack.pop()) if op in ops else float(op))
    print(stack)

print(stack.pop())
