#!/usr/bin/env ruby

stack = []

ops = { '+' => lambda { |a,b| b + a}, '-' => lambda { |a,b| b - a},
        '*' => lambda { |a,b| b * a}, '/' => lambda { |a,b| b / a}}

ARGF.each do |line|
  line.split(/\s+/).each do |op|
    stack.push(ops.has_key?(op) ? ops[op].call(stack.pop, stack.pop) : op.to_f)
  end
  puts "[#{stack.join(",")}]"
end

puts stack.pop
