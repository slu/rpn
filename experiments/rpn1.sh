#!/usr/bin/env bash

set -euo pipefail

declare -a stack
stack_size=0

function push() {
    stack+=("$1")
    stack_size=$((stack_size+1))
}

function pop() {
    stack_size=$((stack_size-=1))
    printf -v "$1" "%s" "${stack[$stack_size]}"
}

function join() {
    local IFS="$1"
    shift; echo "$*"
}

while read -r line; do    
    for op in $line; do
        case $op in
            '+'|'-') pop a; pop b; push 56;;
            *) push "$op";;
        esac        
    done
    echo "[$(join "," "${stack[@]:0:${stack_size}}")]"
done < <(cat "$@")

pop result
echo $result
