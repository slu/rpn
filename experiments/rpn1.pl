#!/usr/bin/env perl

my @stack;

my %ops = ( '+' => sub { $_[1] + $_[0] }, '-' => sub { $_[1] - $_[0] },
            '*' => sub { $_[1] * $_[0] }, '/' => sub { $_[1] / $_[0] });

while (my $line = <>) {
    for my $op (split(/\s+/, $line)) {
        push @stack, exists $ops{$op} ? $ops{$op}->(pop @stack, pop @stack) : $op;
    }
    print STDERR "[", join(',', @stack), "]\n";
}

print pop @stack,"\n";
